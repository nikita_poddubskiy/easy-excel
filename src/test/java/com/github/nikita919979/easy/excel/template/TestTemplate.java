package com.github.nikita919979.easy.excel.template;

import com.github.nikita919979.easy.excel.annotation.ExcelColumn;
import com.github.nikita919979.easy.excel.annotation.ExcelHeader;
import com.github.nikita919979.easy.excel.annotation.ExcelInfo;
import com.github.nikita919979.easy.excel.annotation.ExcelSheet;
import com.github.nikita919979.easy.excel.model.type.FontType;
import lombok.Data;

import java.util.List;

@Data
@ExcelInfo
public class TestTemplate {

    @ExcelSheet(name = "Sheet_1")
    private List<TestSheet1> testSheet1;
    @ExcelSheet(name = "Sheet_2")
    private List<TestSheet2> sheet2;

    @Data
    public static class TestSheet1 {
        @ExcelHeader(name = "Sheet1_field1")
        @ExcelColumn(order = 1)
        private String field1;
        @ExcelHeader(name = "Sheet1_field2")
        @ExcelColumn(font = FontType.ARIAL_BLACK, order = 2)
        private String field2;
    }

    @Data
    public static class TestSheet2 {
        @ExcelColumn(order = 2)
        private String field1;
        @ExcelHeader(name = "Sheet2_field2", font = FontType.CALIBRI)
        @ExcelColumn(font = FontType.ARIAL_BLACK, order = 1)
        private String field2;
    }
}
