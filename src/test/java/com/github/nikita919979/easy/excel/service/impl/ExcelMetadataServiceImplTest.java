package com.github.nikita919979.easy.excel.service.impl;

import com.github.nikita919979.easy.excel.model.ExcelColumnMetadata;
import com.github.nikita919979.easy.excel.model.ExcelHeaderMetadata;
import com.github.nikita919979.easy.excel.model.ExcelMetadata;
import com.github.nikita919979.easy.excel.model.ExcelSheetMetadata;
import com.github.nikita919979.easy.excel.model.type.ExcelType;
import com.github.nikita919979.easy.excel.model.type.FontType;
import com.github.nikita919979.easy.excel.template.TestTemplate;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ExcelMetadataServiceImplTest {

    private final ExcelMetadataServiceImpl metadataService = new ExcelMetadataServiceImpl();

    @Test
    public void shouldReturnExcelMetadata() {
        //GIVEN
        TestTemplate testTemplate = new TestTemplate();

        //WHEN
        ExcelMetadata result = metadataService.getExcelMetadata(testTemplate.getClass());

        //THEN
        assertEquals("excel_document", result.getFileName());
        assertEquals(ExcelType.XLSX, result.getExtension());

        List<ExcelSheetMetadata> sheetsMetadata = result.getSheetsMetadata();
        ExcelSheetMetadata excelSheetMetadata1 = sheetsMetadata.get(0);
        ExcelSheetMetadata excelSheetMetadata2 = sheetsMetadata.get(1);

        List<ExcelColumnMetadata> columnsMetadata1 = excelSheetMetadata1.getColumnsMetadata();
        assertEquals("Sheet_1", excelSheetMetadata1.getName());
        assertEquals("testSheet1", excelSheetMetadata1.getFieldName());

        ExcelColumnMetadata excelColumnMetadata1Sheet1 = columnsMetadata1.get(0);
        assertEquals(FontType.ARIAL, excelColumnMetadata1Sheet1.getFont());
        assertEquals("field1", excelColumnMetadata1Sheet1.getFieldName());
        assertTrue(excelColumnMetadata1Sheet1.getHeaderExists());
        ExcelHeaderMetadata headerMetadataSheet1Column1 = excelColumnMetadata1Sheet1.getHeaderMetadata();
        assertEquals("Sheet1_field1", headerMetadataSheet1Column1.getName());
        assertEquals(FontType.ARIAL, headerMetadataSheet1Column1.getFont());

        ExcelColumnMetadata excelColumnMetadata2Sheet1 = columnsMetadata1.get(1);
        assertEquals(FontType.ARIAL_BLACK, excelColumnMetadata2Sheet1.getFont());
        assertEquals("field2", excelColumnMetadata2Sheet1.getFieldName());
        assertTrue(excelColumnMetadata2Sheet1.getHeaderExists());
        ExcelHeaderMetadata headerMetadataSheet1Column2 = excelColumnMetadata2Sheet1.getHeaderMetadata();
        assertEquals("Sheet1_field2", headerMetadataSheet1Column2.getName());
        assertEquals(FontType.ARIAL, headerMetadataSheet1Column2.getFont());

        assertEquals("Sheet_2", excelSheetMetadata2.getName());

        List<ExcelColumnMetadata> columnsMetadata2 = excelSheetMetadata2.getColumnsMetadata();
        assertEquals("Sheet_2", excelSheetMetadata2.getName());
        assertEquals("sheet2", excelSheetMetadata2.getFieldName());

        ExcelColumnMetadata excelColumnMetadata1Sheet2 = columnsMetadata2.get(0);
        assertEquals(FontType.ARIAL_BLACK, excelColumnMetadata1Sheet2.getFont());
        assertEquals("field2", excelColumnMetadata1Sheet2.getFieldName());
        assertTrue(excelColumnMetadata1Sheet2.getHeaderExists());
        ExcelHeaderMetadata headerMetadataSheet2Column1 = excelColumnMetadata1Sheet2.getHeaderMetadata();
        assertEquals("Sheet2_field2", headerMetadataSheet2Column1.getName());
        assertEquals(FontType.CALIBRI, headerMetadataSheet2Column1.getFont());

        ExcelColumnMetadata excelColumnMetadata2Sheet2 = columnsMetadata2.get(1);
        assertEquals(FontType.ARIAL, excelColumnMetadata2Sheet2.getFont());
        assertEquals("field1", excelColumnMetadata2Sheet2.getFieldName());
        assertFalse(excelColumnMetadata2Sheet2.getHeaderExists());

        assertEquals("Sheet_2", excelSheetMetadata2.getName());
    }
}
