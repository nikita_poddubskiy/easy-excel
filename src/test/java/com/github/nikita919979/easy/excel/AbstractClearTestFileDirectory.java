package com.github.nikita919979.easy.excel;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.io.TempDir;

import java.io.*;
import java.nio.file.Path;

@Slf4j
public abstract class AbstractClearTestFileDirectory {

    @TempDir
    protected static Path TEMP_DIR;

    protected File tempFile = new File(TEMP_DIR + "\\test_file.xlsx");

    protected void writeToFile(Workbook workbook) {
        try (ByteArrayOutputStream outStream = new ByteArrayOutputStream();
             OutputStream fileOutputStream = new FileOutputStream(tempFile)) {
            workbook.write(outStream);
            outStream.writeTo(fileOutputStream);
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}
