package com.github.nikita919979.easy.excel.component.xlsx;

import com.github.nikita919979.easy.excel.annotation.ExcelColumn;
import com.github.nikita919979.easy.excel.annotation.ExcelInfo;
import com.github.nikita919979.easy.excel.annotation.ExcelSheet;
import com.github.nikita919979.easy.excel.builder.impl.ExcelSheetBuilderImpl;
import com.github.nikita919979.easy.excel.service.impl.ExcelServiceImpl;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
public class ExcelInfoBuilderImplTest {

//    private final ExcelSheetBuilderImpl excelSheetBuilderImpl = new ExcelSheetBuilderImpl();
//    private final ExcelServiceImpl excelServiceImpl = new ExcelServiceImpl(excelSheetBuilderImpl);
//
//    @Test
//    public void shouldCreateExcelWithSpecifiedSheetsWithoutOrder() {
//        //GIVEN
//        TestSheetExcel testMetadataExcel = new TestSheetExcel();
//
//        //WHEN
//        Workbook oneSheetExcel = excelServiceImpl.createExcel(testMetadataExcel, TestSheetExcel.class);
//
//        //THEN
//        assertEquals("Sheet__1", oneSheetExcel.getSheetAt(0).getSheetName());
//        assertEquals("Sheet__2", oneSheetExcel.getSheetAt(1).getSheetName());
//    }
//
//    @Test
//    public void shouldCreateExcelWithSpecifiedSheetsAndOrder() {
//        //GIVEN
//        TestSheetExcelWithOrder testMetadataExcel = new TestSheetExcelWithOrder();
//
//        //WHEN
//        Workbook oneSheetExcel = excelServiceImpl.createExcel(testMetadataExcel, TestSheetExcelWithOrder.class);
//
//        //THEN
//        assertEquals("Sheet__2", oneSheetExcel.getSheetAt(0).getSheetName());
//        assertEquals("Sheet__1", oneSheetExcel.getSheetAt(1).getSheetName());
//    }
//
//    @Test
//    public void testBoss() throws IOException {
//        //GIVEN
//        TestSheetExcelWithOrder testMetadataExcel = new TestSheetExcelWithOrder();
//        TestSheet1WithOrder testSheet1WithOrder = new TestSheet1WithOrder();
//        testSheet1WithOrder.setField1("testField1");
//        testSheet1WithOrder.setField2("testField2");
//
//        TestSheet2WithOrder testSheet2WithOrder = new TestSheet2WithOrder();
//        testSheet2WithOrder.setField1("testField3");
//        testSheet2WithOrder.setField2("testField4");
//
//        testMetadataExcel.setSheet1(Collections.singletonList(testSheet1WithOrder));
//        testMetadataExcel.setSheet2(Collections.singletonList(testSheet2WithOrder));
//
//        //WHEN
//        Workbook oneSheetExcel = excelServiceImpl.createExcel(testMetadataExcel, TestSheetExcelWithOrder.class);
//        //THEN
//        try (FileOutputStream outputStream = new FileOutputStream("/test_excel.xlsx")) {
//            oneSheetExcel.write(outputStream);
//        }
//    }
}