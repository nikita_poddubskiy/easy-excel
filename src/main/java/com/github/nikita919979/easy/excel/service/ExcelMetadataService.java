package com.github.nikita919979.easy.excel.service;

import com.github.nikita919979.easy.excel.model.ExcelMetadata;

public interface ExcelMetadataService {

    <T> ExcelMetadata getExcelMetadata(Class<T> templateClass);
}
