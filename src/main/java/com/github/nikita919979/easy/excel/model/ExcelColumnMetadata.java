package com.github.nikita919979.easy.excel.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExcelColumnMetadata {

    private ExcelHeaderMetadata headerMetadata;
    private String font;
    private Boolean headerExists;
    private String fieldName;
}
