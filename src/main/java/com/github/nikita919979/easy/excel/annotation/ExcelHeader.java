package com.github.nikita919979.easy.excel.annotation;

import com.github.nikita919979.easy.excel.model.type.FontType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelHeader {

    String name() default "";

    String font() default FontType.ARIAL;
}
