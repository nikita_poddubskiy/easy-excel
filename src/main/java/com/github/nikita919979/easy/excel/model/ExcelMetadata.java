package com.github.nikita919979.easy.excel.model;

import com.github.nikita919979.easy.excel.model.type.ExcelType;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ExcelMetadata {

    private String fileName;
    private ExcelType extension;
    private List<ExcelSheetMetadata> sheetsMetadata;
}
