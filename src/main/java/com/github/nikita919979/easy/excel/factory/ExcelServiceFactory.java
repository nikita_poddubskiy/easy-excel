package com.github.nikita919979.easy.excel.factory;

import com.github.nikita919979.easy.excel.builder.impl.ExcelColumnBuilderImpl;
import com.github.nikita919979.easy.excel.builder.impl.ExcelHeaderBuilderImpl;
import com.github.nikita919979.easy.excel.builder.impl.ExcelSheetBuilderImpl;
import com.github.nikita919979.easy.excel.service.ExcelService;
import com.github.nikita919979.easy.excel.service.impl.ExcelMetadataServiceImpl;
import com.github.nikita919979.easy.excel.service.impl.ExcelServiceImpl;

public class ExcelServiceFactory {

    public ExcelService getDefaultExcelService() {
        return new ExcelServiceImpl(new ExcelMetadataServiceImpl(),
                new ExcelSheetBuilderImpl(new ExcelColumnBuilderImpl(new ExcelHeaderBuilderImpl())));
    }
}
