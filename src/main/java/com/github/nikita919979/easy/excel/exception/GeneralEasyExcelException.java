package com.github.nikita919979.easy.excel.exception;

public class GeneralEasyExcelException extends RuntimeException {

    public GeneralEasyExcelException() {
    }

    public GeneralEasyExcelException(String message) {
        super(message);
    }

    public GeneralEasyExcelException(String message, Throwable cause) {
        super(message, cause);
    }
}
