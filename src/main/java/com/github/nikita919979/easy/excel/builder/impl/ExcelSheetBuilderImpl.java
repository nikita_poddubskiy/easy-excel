package com.github.nikita919979.easy.excel.builder.impl;

import com.github.nikita919979.easy.excel.builder.ExcelColumnBuilder;
import com.github.nikita919979.easy.excel.builder.ExcelSheetBuilder;
import com.github.nikita919979.easy.excel.exception.GeneralEasyExcelException;
import com.github.nikita919979.easy.excel.model.ExcelMetadata;
import com.github.nikita919979.easy.excel.model.ExcelSheetMetadata;
import com.github.nikita919979.easy.excel.model.type.ExcelType;
import lombok.RequiredArgsConstructor;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;

@RequiredArgsConstructor
public class ExcelSheetBuilderImpl implements ExcelSheetBuilder {

    private final ExcelColumnBuilder columnBuilder;

    @Override
    public <T> Workbook buildSheets(T template, Class<T> templateClass, ExcelMetadata metadata) {
        Workbook workbook;
        if (metadata.getExtension() == ExcelType.XLSX) {
            workbook = new SXSSFWorkbook();
        } else {
            workbook = new HSSFWorkbook();
        }
        try {
            createSheets(workbook, templateClass, template, metadata);
        } catch (Exception e) {
            throw new GeneralEasyExcelException(e.getMessage(), e);
        }
        return workbook;
    }

    private <T> void createSheets(Workbook workbook, Class<T> templateClass, T template, ExcelMetadata metadata)
            throws Exception {
        for (ExcelSheetMetadata sheetMetadata : metadata.getSheetsMetadata()) {
            String fieldName = sheetMetadata.getFieldName();
            Field sheetsField = templateClass.getField(fieldName);
            createSheet(workbook, sheetMetadata, sheetsField, template);
        }
    }

    private <T, C> void createSheet(Workbook workbook, ExcelSheetMetadata sheetMetadata, Field sheetsField,
                                    T template) throws Exception {
        ParameterizedType paramCollectionType = (ParameterizedType) sheetsField.getGenericType();
        Class<C> contentClass = (Class<C>) paramCollectionType.getActualTypeArguments()[0];
        Collection<C> content = (Collection<C>) sheetsField.get(template);
        Sheet sheet = workbook.createSheet(sheetMetadata.getName());
        columnBuilder.buildColumns(workbook, sheet, contentClass, content, sheetMetadata);
    }
}
