package com.github.nikita919979.easy.excel.service;

import org.apache.poi.ss.usermodel.Workbook;

public interface ExcelService {

    <T> Workbook createExcel(T template, Class<T> templateClass);
}
