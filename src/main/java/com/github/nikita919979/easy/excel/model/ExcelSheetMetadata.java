package com.github.nikita919979.easy.excel.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ExcelSheetMetadata {

    private String name;
    private List<ExcelColumnMetadata> columnsMetadata;
    private String fieldName;
}
