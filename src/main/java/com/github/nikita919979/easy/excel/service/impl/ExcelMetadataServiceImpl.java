package com.github.nikita919979.easy.excel.service.impl;

import com.github.nikita919979.easy.excel.annotation.ExcelColumn;
import com.github.nikita919979.easy.excel.annotation.ExcelHeader;
import com.github.nikita919979.easy.excel.annotation.ExcelInfo;
import com.github.nikita919979.easy.excel.annotation.ExcelSheet;
import com.github.nikita919979.easy.excel.model.ExcelColumnMetadata;
import com.github.nikita919979.easy.excel.model.ExcelHeaderMetadata;
import com.github.nikita919979.easy.excel.model.ExcelMetadata;
import com.github.nikita919979.easy.excel.model.ExcelSheetMetadata;
import com.github.nikita919979.easy.excel.service.ExcelMetadataService;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class ExcelMetadataServiceImpl implements ExcelMetadataService {

    @Override
    public <T> ExcelMetadata getExcelMetadata(Class<T> templateClass) {
        ExcelMetadata metadata = new ExcelMetadata();

        ExcelInfo metadataAnnotation = templateClass.getAnnotation(ExcelInfo.class);
        if (metadataAnnotation != null) {
            metadata.setFileName(metadataAnnotation.fileName());
            metadata.setExtension(metadataAnnotation.extension());
            metadata.setSheetsMetadata(getSheetsMetadata(templateClass));
        }
        return metadata;
    }

    private <T, C> List<ExcelSheetMetadata> getSheetsMetadata(Class<T> excelTemplate) {
        List<ExcelSheetMetadata> sheetsMetadata = new ArrayList<>();

        Arrays.stream(excelTemplate.getDeclaredFields())
                .filter(sheetField -> sheetField.getAnnotation(ExcelSheet.class) != null)
                .filter(sheetField -> Collection.class.isAssignableFrom(sheetField.getType()))
                .sorted(Comparator.comparingInt(field -> field.getAnnotation(ExcelSheet.class).order()))
                .forEach(sheetField -> {
                    ExcelSheetMetadata sheetMetadata = new ExcelSheetMetadata();
                    ExcelSheet sheetInfo = sheetField.getAnnotation(ExcelSheet.class);
                    sheetMetadata.setName(sheetInfo.name());

                    ParameterizedType paramCollectionType = (ParameterizedType) sheetField.getGenericType();
                    Class<C> collectionType = (Class<C>) paramCollectionType.getActualTypeArguments()[0];
                    sheetMetadata.setColumnsMetadata(getColumnsMetadata(collectionType));

                    sheetMetadata.setFieldName(sheetField.getName());
                    sheetsMetadata.add(sheetMetadata);
                });

        return sheetsMetadata;
    }

    private <T> Stream<Field> getAvailableColumns(Class<T> sheetTemplate) {
        return Arrays.stream(sheetTemplate.getDeclaredFields())
                .sorted(Comparator.comparingInt(field -> field.getAnnotation(ExcelColumn.class).order()))
                .filter(columnField -> columnField.getAnnotation(ExcelColumn.class) != null);
    }

    private <T> List<ExcelColumnMetadata> getColumnsMetadata(Class<T> sheetTemplate) {
        List<ExcelColumnMetadata> columnsMetadata = new ArrayList<>();
        getAvailableColumns(sheetTemplate)
                .sorted(Comparator.comparingInt(field -> field.getAnnotation(ExcelColumn.class).order()))
                .forEach(columnField -> {
                    ExcelColumn columnInfo = columnField.getAnnotation(ExcelColumn.class);
                    ExcelColumnMetadata columnMetadata = new ExcelColumnMetadata();
                    columnMetadata.setFont(columnInfo.font());
                    ExcelHeaderMetadata headerMetadata = getHeaderMetadata(columnField);
                    columnMetadata.setHeaderMetadata(headerMetadata);
                    columnMetadata.setHeaderExists(headerMetadata != null);
                    columnMetadata.setFieldName(columnField.getName());
                    columnsMetadata.add(columnMetadata);
                });

        return columnsMetadata;
    }

    private ExcelHeaderMetadata getHeaderMetadata(Field columnField) {
        ExcelHeader headerInfo = columnField.getAnnotation(ExcelHeader.class);
        if (headerInfo != null) {
            ExcelHeaderMetadata headerMetadata = new ExcelHeaderMetadata();
            headerMetadata.setName(headerInfo.name());
            headerMetadata.setFont(headerInfo.font());
            return headerMetadata;
        }
        return null;
    }
}
