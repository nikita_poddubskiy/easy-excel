package com.github.nikita919979.easy.excel.service.impl;

import com.github.nikita919979.easy.excel.builder.ExcelSheetBuilder;
import com.github.nikita919979.easy.excel.model.ExcelMetadata;
import com.github.nikita919979.easy.excel.model.type.ExcelType;
import com.github.nikita919979.easy.excel.service.ExcelMetadataService;
import com.github.nikita919979.easy.excel.service.ExcelService;
import lombok.RequiredArgsConstructor;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

@RequiredArgsConstructor
public class ExcelServiceImpl implements ExcelService {

    private final ExcelMetadataService metadataService;
    private final ExcelSheetBuilder sheetBuilder;

    @Override
    public <T> Workbook createExcel(T template, Class<T> templateClass) {
        ExcelMetadata excelMetadata = metadataService.getExcelMetadata(templateClass);
        Workbook workbook = null;
        if (!excelMetadata.getSheetsMetadata().isEmpty()) {
            if (excelMetadata.getExtension() == ExcelType.XLSX) {
                workbook = sheetBuilder.buildSheets(template, templateClass, excelMetadata);
            } else {

            }
        } else {
            if (excelMetadata.getExtension() == ExcelType.XLSX) {
                workbook = new SXSSFWorkbook();
            } else {
                workbook = new HSSFWorkbook();
            }
        }
        return workbook;
    }
}
