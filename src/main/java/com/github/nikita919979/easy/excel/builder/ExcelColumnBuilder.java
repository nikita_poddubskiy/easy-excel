package com.github.nikita919979.easy.excel.builder;

import com.github.nikita919979.easy.excel.model.ExcelSheetMetadata;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.Collection;

public interface ExcelColumnBuilder {

    <C> void buildColumns(Workbook workbook, Sheet sheet, Class<C> contentClass, Collection<C> content,
                          ExcelSheetMetadata sheetMetadata);
}
