package com.github.nikita919979.easy.excel.builder;

import com.github.nikita919979.easy.excel.model.ExcelMetadata;
import org.apache.poi.ss.usermodel.Workbook;

public interface ExcelSheetBuilder {

    <T> Workbook buildSheets(T template, Class<T> templateClass, ExcelMetadata metadata);
}
