package com.github.nikita919979.easy.excel.builder;

import com.github.nikita919979.easy.excel.model.ExcelHeaderMetadata;
import com.github.nikita919979.easy.excel.model.RowColumn;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public interface ExcelHeaderBuilder {
    RowColumn buildHeader(Workbook workbook, Sheet sheet, RowColumn rowColumn, ExcelHeaderMetadata headerMetadata);
}
