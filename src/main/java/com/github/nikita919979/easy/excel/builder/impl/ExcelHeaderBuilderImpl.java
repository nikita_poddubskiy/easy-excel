package com.github.nikita919979.easy.excel.builder.impl;

import com.github.nikita919979.easy.excel.builder.ExcelHeaderBuilder;
import com.github.nikita919979.easy.excel.model.ExcelHeaderMetadata;
import com.github.nikita919979.easy.excel.model.RowColumn;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class ExcelHeaderBuilderImpl implements ExcelHeaderBuilder {

    public RowColumn buildHeader(Workbook workbook, Sheet sheet, RowColumn rowColumn,
                                 ExcelHeaderMetadata headerMetadata) {
        if (headerMetadata != null) {
            Row row = sheet.getRow(rowColumn.getRow());
            if (row == null) {
                row = sheet.createRow(rowColumn.getRow());
            }
            Cell cell = row.createCell(rowColumn.getColumn());
            cell.setCellValue(headerMetadata.getName());
            cell.setCellStyle(createHeaderCellStyle(workbook, headerMetadata));
            rowColumn.setRow(1);
            rowColumn.setColumn(1);
        }
        return rowColumn;
    }

    private CellStyle createHeaderCellStyle(Workbook workbook, ExcelHeaderMetadata headerMetadata) {
        CellStyle cellStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName(headerMetadata.getFont());
        cellStyle.setFont(font);
        return cellStyle;
    }
}
