package com.github.nikita919979.easy.excel.annotation;

import com.github.nikita919979.easy.excel.model.type.ExcelType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelInfo {

    String fileName() default "excel_document";

    ExcelType extension() default ExcelType.XLSX;
}
