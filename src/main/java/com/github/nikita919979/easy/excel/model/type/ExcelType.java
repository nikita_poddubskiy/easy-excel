package com.github.nikita919979.easy.excel.model.type;

import lombok.Getter;

@Getter
public enum ExcelType {
    XLSX(".xlsx"), XLS(".xls");

    private final String extension;

    ExcelType(String extension) {
        this.extension = extension;
    }
}
