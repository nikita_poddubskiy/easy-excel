package com.github.nikita919979.easy.excel.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExcelHeaderMetadata {

    private String name;
    private String font;
}
