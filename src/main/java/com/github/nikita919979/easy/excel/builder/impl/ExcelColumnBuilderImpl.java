package com.github.nikita919979.easy.excel.builder.impl;

import com.github.nikita919979.easy.excel.builder.ExcelColumnBuilder;
import com.github.nikita919979.easy.excel.builder.ExcelHeaderBuilder;
import com.github.nikita919979.easy.excel.model.ExcelColumnMetadata;
import com.github.nikita919979.easy.excel.model.ExcelHeaderMetadata;
import com.github.nikita919979.easy.excel.model.ExcelSheetMetadata;
import com.github.nikita919979.easy.excel.model.RowColumn;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.Collection;
import java.util.List;

@RequiredArgsConstructor
public class ExcelColumnBuilderImpl implements ExcelColumnBuilder {

    private final ExcelHeaderBuilder headerBuilder;

    @Override
    public <C> void buildColumns(Workbook workbook, Sheet sheet, Class<C> contentClass, Collection<C> content,
                                 ExcelSheetMetadata sheetMetadata) {
        List<ExcelColumnMetadata> columnsMetadata = sheetMetadata.getColumnsMetadata();
        RowColumn rowColumn = new RowColumn();
        for (ExcelColumnMetadata columnMetadata : columnsMetadata) {
            buildColumn(workbook, sheet, content, columnMetadata, rowColumn);
            rowColumn.setRow(0);
            rowColumn.setColumn(rowColumn.getColumn() + 1);
        }
    }

    private <C> void buildColumn(Workbook workbook, Sheet sheet, Collection<C> content,
                                 ExcelColumnMetadata columnMetadata, RowColumn rowColumn) {
        ExcelHeaderMetadata headerMetadata = columnMetadata.getHeaderMetadata();
        headerBuilder.buildHeader(workbook, sheet, rowColumn, headerMetadata);
        for (C value : content) {
            createCell(workbook, sheet, value, columnMetadata, rowColumn);
        }
    }

    private void createCell(Workbook workbook, Sheet sheet, Object value, ExcelColumnMetadata columnMetadata,
                            RowColumn rowColumn) {
        int rowNumber = rowColumn.getRow();
        Row row = sheet.getRow(rowNumber);
        if (row == null) {
            row = sheet.createRow(rowNumber);
        }
        Cell cell = row.createCell(rowColumn.getColumn());
        cell.setCellStyle(createCellStyle(workbook, columnMetadata));
        cell.setCellValue(value.toString());
        rowColumn.setRow(rowNumber + 1);
    }

    private CellStyle createCellStyle(Workbook workbook, ExcelColumnMetadata columnMetadata) {
        CellStyle cellStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName(columnMetadata.getFont());
        cellStyle.setFont(font);
        return cellStyle;
    }
}
